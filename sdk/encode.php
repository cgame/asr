<?php

/**
 * 
 * 使用 ffmpeg 将 sample/res 目录下的样本文件切割
 * 
 */

include_once "init.php";
define('RES_PATH',ROOT_PATH.'/sample/res/');

// $RES_PATH = $ROOT_PATH.'res/';

$files = getm4as(RES_PATH);
// var_dump($files);

function getString($time){
  $m = floor($time / 60);
  $i = $time % 60;

  return sprintf("%s:%s",$m < 10 ? '0'.$m:$m,$i < 10 ? '0'.$i:$i);
}

function getTimer($str){
  $array = preg_split('/:/',$str);
  // sprintf("%s".PHP_EOL,$array[0]);
  return intval($array[0]) * 60 + intval($array[1]);
}

function splix($file){

  $txt = file_get_contents(RES_PATH.$file['filename'].'.txt');
  $lines = preg_split('/\\n/',$txt);
  // var_dump($matches[1]);
  $len = sizeof($lines);

  for($i = 0;$i < $len - 1;$i++){
    // echo substr($lines[$i],0,5);
    $stime = substr($lines[$i],0,5);
    $etime = substr($lines[$i + 1],0,5);
    $dur = getTimer($etime) - getTimer($stime);
    
    // printf("stime:%s,dur:%s".PHP_EOL,$stime,getString($dur));
    // print_r()
    $cmd = sprintf('docker exec e7ffmpeg ffmpeg -i /root/sample/res/%s.m4a -y -dn -vn -ar 16000 -ac 1 -ss 00:%s -t 00:%s -ab 64k -f wav /root/sample/1x/%s-%d.wav',$file['filename'],$stime,getString($dur),$file['filename'],$i + 1);
    // printf("%s".PHP_EOL,$cmd);
    system($cmd);
  }
}

foreach($files as $file){
  splix($file);
  // break;
}
?>