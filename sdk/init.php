<?php

/**
 * 
 * 默认配置的设定与工具函数的声明
 * 
 */

define('DS', DIRECTORY_SEPARATOR);
define('ROOT_PATH', dirname(realpath(__DIR__)) . DS);
define('RESULT_PATH',ROOT_PATH .'result'. DS);
define('OUT_PATH', ROOT_PATH .'out'. DS);

$defaults = array(
  'format'=>'wav'
);

function exhandler ( Exception $ex ){
  echo "Uncaught exception: " , $exception->getMessage(), "\n";
}

set_exception_handler("exhandler");

function getm4as($dir){
  $files = scandir($dir);
  $returns = array();
  foreach($files as $file){
    // var_dump($file);
    if(preg_match('/\.m4a$/',$file)){
      // echo $file;
      $returns[] = array(
        'filepath'=>$dir.'/'.$file,
        'filename'=>substr($file,0,strlen($file)-4)
      );
    }
  }
  return $returns;
}

function getfiles($dir){
  $files = scandir($dir);
  $returns = array();
  foreach($files as $file){
    // var_dump($file);
    if(preg_match('/\.wav$/',$file)){
      // echo $file;
      $returns[] = array(
        'filepath'=>$dir.'/'.$file,
        'filename'=>substr($file,0,strlen($file)-4)
      );
    }
  }
  return $returns;
}

function put_rusult($filename,$data,$flags = 0){
  $dir = dirname($filename);
  
  if(!is_dir($dir) && mkdir($dir,true)){
    echo $dir;
    return;
  }
  // echo
  file_put_contents($filename,$data);
}